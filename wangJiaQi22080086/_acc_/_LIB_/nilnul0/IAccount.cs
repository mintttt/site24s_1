﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul
{
    interface IAccount
    {
    }

    /// <summary>
    /// membered interface
    /// </summary>

    interface AccountI:IAccount
    {
        /// <summary>
        /// username
        /// </summary>
        string name { get;  }
    }
}
