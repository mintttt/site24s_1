﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul._acc
{
    /// <summary>
    /// list item of different types of account
    /// </summary>
    enum Li_
    {
        /// <summary>
        /// only a password, no username;
        /// eg:
        ///     elock, phone screen unlok,token(one time password)
        /// </summary>
        Pass
            ,
        /// <summary>
        ///a username and a password 
        /// </summary>
        Login
            ,
        /// <summary>
        ///sms code to verify
        /// </summary>
        Phone
            ,
        Face
            ,
        FingerPrint


    }
}
