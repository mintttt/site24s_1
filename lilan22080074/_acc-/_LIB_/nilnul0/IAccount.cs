﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace nilnul
{
    /// <summary>
    /// marker interface
    /// </summary>
    interface IAccount
    {
    }
    /// <summary>
    /// membered intterface
    /// </summary>
    interface AccountI : IAccount
    {
        /// <summary>
        /// username
        /// </summary>
        string username { get; }
    }
}
