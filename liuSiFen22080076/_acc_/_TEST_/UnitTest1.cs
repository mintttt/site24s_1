﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;

namespace _TEST_
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestMethod1()
        {
            var p = new nilnul.acc.nilnul0.acc_.Pass("123456");

            var check = new nilnul.acc.nilnul0.acc_.pass.LogOn(
                   (string pass) =>pass.Contains("123456")?true:false
                );
            Assert.IsTrue(check(p.val));
            Assert.IsTrue(check("abc12345678"));
            Assert.IsFalse(check("abc123"));
            check(p.val);
        }
    }
}
